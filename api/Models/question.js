const mongoose = require('mongoose')

const AnswerSchema = mongoose.Schema({ answer: Array })

const questionSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    question: { type: String, reuqired: true },
    category: { type: String, required: true },
    difficultyLevel: { type: Number, default: 1 },
    experiencePoints: { type: Number, default: 1 }
})

module.exports = mongoose.model('Question', questionSchema)