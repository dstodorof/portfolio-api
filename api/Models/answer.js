const mongoose = require('mongoose')

const answerSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    answer: { type: String, required: true},
    questionId: { type: mongoose.Schema.Types.ObjectId, ref: 'Question', required: true },
    isCorrect: { type: Boolean, default: false }
})

module.exports = mongoose.model('Answer', answerSchema)