const mongoose = require('mongoose')

const Question = require('../Models/question')
const Answer = require('../Models/answer')

exports.get_all = (req, res, next) => {
    Question
        .find()
        .select('_id question category difficultyLevel experiencePoints ')
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                questions: docs.map(doc => {
                    return {
                        id: doc._id,
                        question: doc.question,
                        category: doc.category,
                        difficultyLevel: doc.difficultyLevel,
                        experiencePoints: doc.experiencePoints
                    }
                })
            }
            if (docs.length >= 1) {
                res.status(200).json(response)
            } else {
                res.status(404).json({
                    message: 'No entries found'
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
}

exports.get_single = (req, res, next) => {
    const id = req.params.questionId
    Question
        .findById(id)
        .select('_id question category difficultyLevel experiencePoints ')
        .exec()
        .then(doc => {
            if (doc) {
                res.status(200).json({
                    question: doc
                })
            } else {
                res.status(400).json({
                    message: 'No valid entry found for provided ID'
                })
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

exports.create_question = (req, res, next) => {
    const question = new Question({
        _id: new mongoose.Types.ObjectId(),
        question: req.body.question,
        category: req.body.category,
        difficultyLevel: req.body.difficultyLevel,
        experiencePoints: req.body.experiencePoints
    })

    question
        .save()
        .then(result => {
            res.status(201).json({
                message: 'Question successfulfy created!',
                questionId: result._id
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

exports.update_question = (req, res, next) => {
    const id = req.params.questionId
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }
    Question
        .updateOne({ _id: id }, { $set: updateOps })
        .exec()
        .then(result => {
            if (result.nModified > 0) {
                res.status(200).json({
                    message: 'Product updated'
                })
            } else {
                if (result.n > 0) {
                    res.status(400).json({
                        message: 'No changes have been made to the object'
                    })
                } else {
                    res.status(400).json({
                        message: 'No valid entry found for provided ID'
                    })
                }
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

exports.delete_question = (req, res, next) => {
    const id = req.params.questionId
    Question
        .remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Question deleted'
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

exports.mass_load_question = (req, res, next) => {
    var questionIds = ""
    for(const question of req.body) {
        const newQuestion = new Question({
            _id: new mongoose.Types.ObjectId(),
            question: question.question,
            category: question.category,
            difficultyLevel: question.difficultyLevel,
            experiencePoints: question.experiencePoints
        })

        newQuestion
            .save()
            .then( result => {
                var isCorrectAnswer = false
                questionIds += result._id

                if(question.correctAnswer === question.answer1) {
                    isCorrectAnswer = true
                } else {
                    isCorrectAnswer = false
                }
                const newAnswer1 = new Answer({
                    _id: mongoose.Types.ObjectId(),
                    questionId: result._id,
                    answer: question.answer1,
                    isCorrect: isCorrectAnswer
                })
                
                if(question.correctAnswer === question.answer2) {
                    isCorrectAnswer = true
                } else {
                    isCorrectAnswer = false
                }
                const newAnswer2 = new Answer({
                    _id: mongoose.Types.ObjectId(),
                    questionId: result._id,
                    answer: question.answer2,
                    isCorrect: isCorrectAnswer
                })

                if(question.correctAnswer === question.answer3) {
                    isCorrectAnswer = true
                } else {
                    isCorrectAnswer = false
                }
                const newAnswer3 = new Answer({
                    _id: mongoose.Types.ObjectId(),
                    questionId: result._id,
                    answer: question.answer3,
                    isCorrect: isCorrectAnswer
                })

                if(question.correctAnswer === question.answer4) {
                    isCorrectAnswer = true
                } else {
                    isCorrectAnswer = false
                }
                const newAnswer4 = new Answer({
                    _id: mongoose.Types.ObjectId(),
                    questionId: result._id,
                    answer: question.answer4,
                    isCorrect: isCorrectAnswer
                })

                newAnswer1
                    .save()
                    .then()
                    .catch()
                newAnswer2
                    .save()
                    .then()
                    .catch()
                newAnswer3
                    .save()
                    .then()
                    .catch()
                newAnswer4
                    .save()
                    .then()
                    .catch()
            })
            .catch()
    }
    res.status(201).json({
        message: 'Тhe collection of questions has been successfully loaded!'
    })
}