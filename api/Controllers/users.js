const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')


const User = require("../Models/user")

exports.user_getData = (req, res, next) => {
    const email = req.params.userEmail

    User
        .findOne({ 'email': email })
        .select('username email level rank experience')
        .exec()
        .then(doc => {
            if (doc) {
                res.status(200).json({
                    id: doc.id,
                    email: doc.email,
                    username: doc.username,
                    level: doc.level,
                    rank: doc.rank,
                    experience: doc.experience
                })
            } else {
                res.status(404).json({
                    message: 'User email doesn\'t exist'
                })
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

exports.user_signup = (req, res, next) => {
    User.find({ email: req.body.email })
        .exec()
        .then(user => {
            if (user.length >= 1) {
                return res.status(409).json({
                    message: 'Mail exists'
                })
            } else {
                bcrypt.hash(req.body.password, 4, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            error: err
                        })
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            username: req.body.username,
                            email: req.body.email,
                            password: hash,
                            application: req.body.application
                        });
                        user
                            .save()
                            .then(result => {
                                console.log(result)
                                res.status(201).json({
                                    message: "User created"
                                })
                            })
                            .catch(err => {
                                console.log(err)
                                res.status(500).json({
                                    error: err
                                })
                            })
                    }
                })
            }
        })
}

exports.user_login = (req, res, next) => {
    User.find({ email: req.body.email })
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(401).json({
                    message: 'Authentication failed!'
                })
            }
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        message: 'Authentication failed!'
                    })
                }
                if (result && user[0].application === req.body.application) {
                    const token = jwt.sign({
                        email: user[0].email,
                        userId: user[0]._id
                    },
                        process.env.JWT_KEY,
                        {
                            expiresIn: '1h'
                        },
                    )
                    return res.status(200).json({
                        message: 'Authentication successful',
                        email: user[0].email,
                        token: token
                    })
                }
                res.status(401).json({
                    message: 'Authentication failed!'
                })
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })

}

exports.update_user = (req, res, next) => {
    const id = req.params.userId
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }

    User
        .updateOne({ _id: id }, { $set: updateOps })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'User updated'
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

exports.user_delete = (req, res, next) => {
    User.deleteOne({ _id: req.params.userId })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'User deleted'
            })
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })
}