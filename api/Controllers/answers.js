const mongoose = require('mongoose')

const Answer = require('../Models/answer')
const Question = require('../Models/question')


exports.create_answer = (req, res, next) => {
    Question.findById(req.body.questionId)
        .then(question => {
            if (!question) {
                return res.status(404).json({
                    message: 'Question not found'
                })
            }
            const answer = new Answer({
                _id: mongoose.Types.ObjectId(),
                questionId: req.body.questionId,
                answer: req.body.answer,
                isCorrect: req.body.isCorrect
            })
            return answer
                .save()
        })
        .then(result => {
            res.status(201).json({
                message: 'Answer is successfully created!'
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

exports.update_answer = (req, res, next) => {
    const id = req.params.answerId
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }

    Answer
        .updateOne({ _id: id }, { $set: updateOps })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Answer updated!'
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

exports.delete_answer_by_question_id = (req, res, next) => {
    const id = req.params.questionId
    Answer
        .deleteMany({ questionId: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Answer deleted!'
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

exports.delete_answer_by_answer_id = (req, res, next) => {
    const id = req.params.answerId
    Answer
        .remove({ _id: id })
        .exec()
        .then(result => {
            console.log(result)
            res.status(200).json({
                message: 'Answer deleted!'
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

exports.get_question_answers = (req, res, next) => {
    const id = req.params.questionId
    var questionText = ""
    Question
        .findOne({ _id: id })
        .select('question')
        .exec()
        .then(result => {
            questionText = result.question
        })
    Answer
        .find({ questionId: id })
        .select('answer isCorrect')
        .exec()
        .then(docs => {
            const response = {
                question: questionText,
                answers : docs.map( doc => {
                    return {
                        answer: doc.answer,
                        isCorrect: doc.isCorrect
                    }
                })
            }
            if (docs.length >= 1) {
                res.status(200).json(response)
            } else {
                res.status(404).json({
                    message: 'No entries found'
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
}

exports.get_all_answers = (req, res, next) => {
    Answer
        .find()
        .select('_id answer questionId isCorrect')
        .exec()
        .then(docs => {
            const response  = {
                count: docs.length, 
                answers: docs.map( doc => {
                    return {
                        id: doc._id,
                        answer: doc.answer,
                        questionId: doc.questionId,
                        isCorrect: doc.isCorrect
                    }
                })
            }
            if (docs.length >= 1) {
                res.status(200).json(response)
            } else {
                res.status(404).json({
                    message: 'No entries found'
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
}