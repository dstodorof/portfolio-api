const express = require('express')
const router = express.Router()

const checkAuth = require('../Middleware/check-auth')
const AnswerController = require('../Controllers/answers')

router.get('/', checkAuth, AnswerController.get_all_answers)

router.post('/', checkAuth, AnswerController.create_answer)

router.patch('/:answerId', checkAuth, AnswerController.update_answer)

router.delete('/byQuestion/:questionId', checkAuth, AnswerController.delete_answer_by_question_id)

router.delete('/byAnswer/:answerId', checkAuth, AnswerController.delete_answer_by_answer_id)

router.get('/:questionId', checkAuth, AnswerController.get_question_answers)

module.exports = router