const express = require('express')
const router = express.Router()

const checkAuth = require('../Middleware/check-auth')
const QuestionController = require('../Controllers/questions')

router.get('/', checkAuth, QuestionController.get_all)

router.get('/:questionId', checkAuth, QuestionController.get_single)

router.post('/', checkAuth, QuestionController.create_question)

router.patch('/:questionId', checkAuth, QuestionController.update_question)

router.delete('/:questionId', checkAuth, QuestionController.delete_question)

router.post('/massload', checkAuth, QuestionController.mass_load_question)

module.exports = router