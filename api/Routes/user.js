const express = require('express')
const router = express.Router()

const checkAuth = require('../Middleware/check-auth')
const UsersControllers = require('../Controllers/users')

router.get('/:userEmail', checkAuth, UsersControllers.user_getData)

router.post('/signup', UsersControllers.user_signup)

router.post('/login', UsersControllers.user_login)

router.patch('/:userId', checkAuth, UsersControllers.update_user)

router.delete('/:userId', checkAuth, UsersControllers.user_delete)

module.exports = router