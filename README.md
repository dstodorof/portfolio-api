# Portfolio projects API

Portfolio-API is an API that is used to exchange information between Portfolio-Android-Apps and MongoDB database.

# API Routes
### User
 - GET - /user/:userId - get user data
 - POST - /user/signup - create new user
 - POST - /user/login - login user, return token
 - PATCH - /user/:userId - update details of the user
 - DELETE - /user/:userId - delete user
  
 
### Question
 - GET - /question - get all questions
 - GET - /question/:questionId - get question
 - POST - /question - create 
 - PATCH - /question/:questionId - update question
 - DELETE - /question/:questionId - delete question
 - POST - /question/massload - Load n count of questions with answers
 
### Answer
 - POST - /answer - add answer - add new answer
 - PATCH - /answer/:answerId - update answer
 - DELETE - /answer/:answerId - delete answer by :answerId
 - DELETE - /answer/:questionId - delete answers by :questionId
 - GET - /answer/:questionId - get question with answers

# Object Attributes

### User
 |Name|Type|Default Value|Required|
 |----|----|-------------|--------|
 |Username|String|-|Yes|
 |Email|String|-|Yes|
 |Password|String|-|Yes|
 |Experience|Number|1|No|
 |Rank|String|New player|No|
 |Level|String|1|No|
 |Application|String|-|Yes|
 
 ### Question
 |Name|Type|Default Value|Required|
 |----|----|-------------|--------|
 |Question|String|-|Yes|
 |Category|String|-|Yes|
 |DifficultLevel|Number|1|No|
 |ExperiencePoints|Number|1|No|
 
  ### Answer
 |Name|Type|Default Value|Required|
 |----|----|-------------|--------|
 |QuestionId|ObjectId|-|Yes|
 |Answer|String|-|Yes|
 |IsCorrect|Boolean|false|No|