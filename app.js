const express = require('express')
const app = express()
const mongoose = require('mongoose')
const morgan = require('morgan')
const bodyParser = require('body-parser')

//IMPORT ROUTES
const userRoutes = require('./api/Routes/user')
const questionRouter = require('./api/Routes/question')
const answerRouter = require('./api/Routes/answer')

//MONGODB CONNECTION AND SETTINGS
mongoose.connect('mongodb://' + process.env.MONGO_ATLAS_USER + ':' + process.env.MONGO_ATLAS_PASS + '@portfolio-api-cluster-shard-00-00-9bklr.mongodb.net:27017,portfolio-api-cluster-shard-00-01-9bklr.mongodb.net:27017,portfolio-api-cluster-shard-00-02-9bklr.mongodb.net:27017/test?ssl=true&replicaSet=portfolio-api-cluster-shard-0&authSource=admin&retryWrites=true', {
    useNewUrlParser: true
})
mongoose.set('useCreateIndex', true)

app.use(morgan('dev'))

//BODYPARSER SETTINGS
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
mongoose.Promise = global.Promise

//REQUEST SETTINGS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Mothods', 'PUT, POST, PATCH, DELETE, GET')
        return res.status(200).json({})
    }
    next()
})

//ACTIVATE ROUTES
app.use('/user', userRoutes)
app.use('/question', questionRouter)
app.use('/answer', answerRouter)

//MISSING ROUTE ERROR
app.use((req, res, next) => {
    const error = new Error('Not found!')
    error.status = 404
    next(error)
})

app.use((error, req, res, next) => {
    res.status(error.status || 500)
    res.json({
        error: {
            message: error.message
        }
    })
})

module.exports = app